<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function construct()
    {
        $this->middleware('auth')-except(['index', 'show']);
    }
    
    public funtion index()
    {
        $post = Post::all();
        return view('post.index', compact('post'));
    }

    public function create()
    {
      return view('post.create');
    }
}
